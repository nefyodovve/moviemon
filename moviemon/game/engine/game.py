from moviemon.settings import \
    GRID_SIZE, PLAYER_INIT_X, PLAYER_INIT_Y, \
    MOVIEBALLS_INIT, MOVIEMON_ENCOUNTER_RATE

import pickle
import random

import requests
import json

def init_moviemon(dict_moviemon):
    """инициируем мувимона из словаря апи в наш словарь с нужными полями"""
    moviemon = dict()
    dict_params = {
        "name" : "Title",
        "id" : "imdbID",
        "strength" : "imdbRating",
        "date" : "Released",
        "director" : "Director",
        "writer" : "Writer",
        "image" : "Poster",
    }
    for key in dict_params:
        moviemon[key] = dict_moviemon[dict_params[key]]
        # moviemon.setdefault(key, []).append(dict_moviemon[dict_params[key]]) (если значений больше чем один)
    moviemon['catched'] = False
    return(moviemon)

def create_moviemons():
    list_moviemons = list()
    list_id_movie = [
        "tt0060666",
        "tt0463392",
        "tt0803096",
        "tt1333125",
        "tt0120616",
        "tt0062453",
        "tt0078748",
        "tt0198781",
        "tt1740707",
        "tt0034398",
    ]
    for id_movie in list_id_movie:
        json_moviemon = requests.get("http://www.omdbapi.com/?i=" + id_movie + "&apikey=7d498d06")
        list_moviemons.append(init_moviemon(json_moviemon.json()))
        #print(moviemon, "\n")
    return(list_moviemons)


class Cell:
    data = ''
    # contains either:
    # '', 'movieball', 'moviemon'
    player = False # player in this cell?
    
    def __init__(self, data='', player=False):
        self.data = data
        self.player = player
    def __str__(self):
        return self.data

class Game:
    """Core class with game logic"""

    pos_x = 0
    pos_y = 0
    movieballs = 0
    m = [] # map, list of lists of Cell
    message = ''
    moviemons = []
    strength = 0
    
    def __init__(self):
        #self.load_default_settings()
        pass
    
    def load(self, data):
        """data is packed info about game - position, movieballs count..."""
        self.pos_x = data['x']
        self.pos_y = data['y']
        self.movieballs = data['movieballs']
        self.message = data['msg']
        self.moviemons = data['moviemons']
        self.strength = data['strength']
        self.m = []
        m_str = data['m']
        for i in range (0, GRID_SIZE[0]):
            self.m.append([])
            for j in range(0, GRID_SIZE[1]):
                self.m[i].append(Cell(data=m_str[i][j]))
        
        self.m[self.pos_y][self.pos_x].player = True
        

    def dump(self):
        # pack data to dict for pickle
        data = {
            'msg': self.message,
            'x': self.pos_x,
            'y': self.pos_y,
            'movieballs': self.movieballs,
            'moviemons': self.moviemons,
            'strength': self.strength
        }
        m_str = []
        for i in range (0, GRID_SIZE[1]):
            m_str.append([])
            for j in range(0, GRID_SIZE[0]):
                m_str[i].append(str(self.m[i][j]))
        data['m'] = m_str
        return data

    def get_random_movie(self):
        not_catched = []
        for mon in self.moviemons:
            if not mon['catched']:
                not_catched.append(mon)
        if len(not_catched) == 0:
            return None
        i = random.randrange(len(not_catched))
        return not_catched[i]['id']

    def load_default_settings(self):
        self.pos_x = PLAYER_INIT_X
        self.pos_y = PLAYER_INIT_Y
        self.movieballs = MOVIEBALLS_INIT
        self.strength = 1
        self.m = []
        for i in range (0, GRID_SIZE[1]):
            self.m.append([])
            for j in range(0, GRID_SIZE[0]):
                self.m[i].append(Cell())


        self.m[self.pos_y][self.pos_y] = Cell(player=True)

        try:
            self.moviemons = create_moviemons()
        except Exception:
            self.moviemons = [{'name': 'Manos: The Hands of Fate', 'id': 'tt0060666', 'strength': '1.8', 'date': '15 Nov 1966', 'director': 'Harold P. Warren', 'writer': 'Harold P. Warren', 'image': 'https://m.media-amazon.com/images/M/MV5BYjIxOWMyMzUtNTMzZC00MThjLTljYjQtZjhlOGQzZTkxOTFkXkEyXkFqcGdeQXVyNjMwMjk0MTQ@._V1_SX300.jpg', 'catched': False}, {'name': 'Zombie Nation', 'id': 'tt0463392', 'strength': '2.0', 'date': '05 Oct 2004', 'director': 'Ulli Lommel', 'writer': 'Ulli Lommel', 'image': 'https://m.media-amazon.com/images/M/MV5BMTczNjE5Njk5Ml5BMl5BanBnXkFtZTcwNDc3MDA0MQ@@._V1_SX300.jpg', 'catched': False}, {'name': 'Warcraft', 'id': 'tt0803096', 'strength': '6.8', 'date': '10 Jun 2016', 'director': 'Duncan Jones', 'writer': 'Charles Leavitt, Duncan Jones', 'image': 'https://m.media-amazon.com/images/M/MV5BMjIwNTM0Mzc5MV5BMl5BanBnXkFtZTgwMDk5NDU1ODE@._V1_SX300.jpg', 'catched': False}, {'name': 'Movie 43', 'id': 'tt1333125', 'strength': '4.3', 'date': '25 Jan 2013', 'director': 'Elizabeth Banks, Steven Brill, Steve Carr', 'writer': 'Rocky Russo, Jeremy Sosenko, Ricky Blitt', 'image': 'https://m.media-amazon.com/images/M/MV5BMTg4NzQ3NDM1Nl5BMl5BanBnXkFtZTcwNjEzMjM3OA@@._V1_SX300.jpg', 'catched': False}, {'name': 'The Mummy', 'id': 'tt0120616', 'strength': '7.0', 'date': '07 May 1999', 'director': 'Stephen Sommers', 'writer': 'Stephen Sommers, Lloyd Fonvielle, Kevin Jarre', 'image': 'https://m.media-amazon.com/images/M/MV5BOTJiYjBhZDgtMjhiOC00MTIzLThlNGMtMmI1NjIwM2M3YTI5XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg', 'catched': False}, {'name': 'Viy', 'id': 'tt0062453', 'strength': '7.3', 'date': '27 Nov 1967', 'director': 'Konstantin Ershov, Georgiy Kropachyov', 'writer': 'Konstantin Ershov, Nikolay Gogol, Georgiy Kropachyov', 'image': 'https://m.media-amazon.com/images/M/MV5BMDRhODFlMmItNTc5Mi00YzNjLWI3YmUtMTAwMmQwN2RkNGM4XkEyXkFqcGdeQXVyNjg3MTIwODI@._V1_SX300.jpg', 'catched': False}, {'name': 'Alien', 'id': 'tt0078748', 'strength': '8.4', 'date': '22 Jun 1979', 'director': 'Ridley Scott', 'writer': "Dan O'Bannon, Ronald Shusett", 'image': 'https://m.media-amazon.com/images/M/MV5BMmQ2MmU3NzktZjAxOC00ZDZhLTk4YzEtMDMyMzcxY2IwMDAyXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SX300.jpg', 'catched': False}, {'name': 'Monsters, Inc.', 'id': 'tt0198781', 'strength': '8.1', 'date': '02 Nov 2001', 'director': 'Pete Docter, David Silverman, Lee Unkrich', 'writer': 'Pete Docter, Jill Culton, Jeff Pidgeon', 'image': 'https://m.media-amazon.com/images/M/MV5BMTY1NTI0ODUyOF5BMl5BanBnXkFtZTgwNTEyNjQ0MDE@._V1_SX300.jpg', 'catched': False}, {'name': 'Trolljegeren', 'id': 'tt1740707', 'strength': '7.0', 'date': '29 Oct 2010', 'director': 'André Øvredal', 'writer': 'André Øvredal', 'image': 'https://m.media-amazon.com/images/M/MV5BMTkyMTgxNzIwOF5BMl5BanBnXkFtZTcwNjYyNjM5NA@@._V1_SX300.jpg', 'catched': False}, {'name': 'The Wolf Man', 'id': 'tt0034398', 'strength': '7.3', 'date': '12 Dec 1941', 'director': 'George Waggner', 'writer': 'Curt Siodmak', 'image': 'https://m.media-amazon.com/images/M/MV5BODRmMDBjMTYtZmJiZi00Mzk3LWFhZDAtMDExNDFhMWQzNzEyXkEyXkFqcGdeQXVyNDY2MTk1ODk@._V1_SX300.jpg', 'catched': False}]

        print(self.moviemons)
        

        # generate N movieballs randomly
        # generate M moviemons randomly

        for i in range(0, 10):
            self.place_movieball()
        

    def get_strength(self):
        return self.strength

    def get_movie(self):
        pass

    def move(self, x_shift, y_shift):
        #print('move', x_shift, y_shift)
        #print('BEFORE MOVE player count:', self.count_player())
        #print('rm player', self.pos_y, self.pos_x)
        #print('player pos:', self.m[self.pos_y][self.pos_x])
        self.m[self.pos_y][self.pos_x].player = False
        self.pos_x += x_shift
        self.pos_y += y_shift
        if (self.pos_y < 0):
            self.pos_y = 0
        if (self.pos_x < 0):
            self.pos_x = 0
        if (self.pos_y >= GRID_SIZE[1]):
            self.pos_y = GRID_SIZE[1] - 1
        if (self.pos_x >= GRID_SIZE[0]):
            self.pos_x = GRID_SIZE[0] - 1
        #print('----player count:', self.count_player())
        #print('set player', self.pos_y, self.pos_x)
        self.m[self.pos_y][self.pos_x].player = True        
        #print('AFTER MOVE player count:', self.count_player())
        #print('player pos:', self.m[self.pos_y][self.pos_x])
        if self.get_current() == 'movieball':
            self.clear_current()
            self.movieballs += 1
            self.message = 'Found movieball!'
            self.place_movieball()
        else:
            if self.moviemon_encounter():
                self.message = 'Wild moviemon appeared!'
                return True
            else:
                self.message = ''
            
        return False

    def catch(self, mon_id):
        count_catched = 0
        for mon in self.moviemons:
            if mon['id'] == mon_id:
                mon['catched'] = True
            if mon['catched'] == True:
                count_catched += 1
        if count_catched == len(self.moviemons):
            return True
        return False
        

    def get_current(self):
        return self.m[self.pos_y][self.pos_x].data
    
    def clear_current(self):
        self.m[self.pos_y][self.pos_x].data = ''

    def moviemon_encounter(self):
        if (random.random() <= MOVIEMON_ENCOUNTER_RATE):
            return True
        return False

    def place_movieball(self):
        x = random.randrange(10)
        y = random.randrange(10)
        self.m[y][x] = 'movieball'

        
