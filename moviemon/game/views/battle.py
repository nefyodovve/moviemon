from django.views.generic import TemplateView
from django.shortcuts import redirect, render

from game.engine.game import Game
from game.engine.statemanager import StateManager
import random

class Battle(TemplateView):
    template_name = 'battle.html'

    def get(self, request, moviemon_id, key=None):
        sm = StateManager()
        data = sm.load()
        game = Game()
        game.load(data)
        moviemon = {}
        for mon in game.moviemons:
            if mon['id'] == moviemon_id:
                moviemon = mon
                break

        key = request.GET.get('key')
        catched = False
        c = 50 - (float(mon['strength']) * 7) + (game.get_strength() * 5)
        if c <= 1:
            c = 1
        if c > 90:
            c = 90
        c = float(c / 100)
        if key == 'a':
            game.movieballs -= 1
            
            r = random.random() 
            if r < c:
                catched = True
                game.strength += 1
                win = game.catch(moviemon_id)
                if win:
                    return redirect('title')
                
            elif game.movieballs <= 0:
                sm.save(game.dump())
                return redirect('worldmap')
            
        sm.save(game.dump())
        if (key == 'b' or catched):
            return redirect('worldmap')
        
        context = {
            'movieballs': game.movieballs,
            'strength': mon['strength'],
            'name': mon['name'],
            'chance': c * 100
        }
        return render(request, self.template_name, context)
