from django.shortcuts import redirect, render
from django.views.generic import TemplateView

from game.engine.game import Game
from game.engine.statemanager import StateManager

class Moviedex(TemplateView):

    template_name = 'moviedex.html'

    def get(self, request):
        sm = StateManager()
        data = sm.load()
        game = Game()
        game.load(data)
        key = request.GET.get('key')
        if (key == 'select'):
            return redirect('worldmap')

        context = {
            'mons': game.moviemons
        }
        sm.save(game.dump())
        return render(request, self.template_name, context)
